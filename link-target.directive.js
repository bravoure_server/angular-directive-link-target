(function (){

    'use strict';

    function linkTarget() {

        return {
            restrict: 'A',
            replace: true,

            link: function(scope, elem, attrs) {

                var a = elem[0];

                if(typeof attrs.ngHref != 'undefined' && attrs.ngHref.indexOf("http") >= 0) {
                    var link = get_hostname(attrs.ngHref).replace(/.*?:\/\//g, "");

                    if (link != location.host)
                        a.target = '_blank';
                } else {
                    return null;
                }

                function get_hostname(url) {
                    if (url.indexOf('https') != -1) {
                        var m = url.match(/^https:\/\/[^/]+/);
                        return m ? m[0] : null;
                    } else {
                        var m = url.match(/^http:\/\/[^/]+/);
                        return m ? m[0] : null;
                    }
                }

            }
        }
    }

    angular
        .module('bravoureAngularApp')
        .directive('linkTarget', linkTarget);

})();
