# Bravoure - Angular Directive Link Target

## Use

This Component is used to add target="_blank" to links
 
### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-directive-link-target": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
    
or in the root of the project execute:
    
    ./update
